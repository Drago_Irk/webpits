/**
 * Created by drago on 23/01/16.
 */
// MAIN

// standard global variables
var container, scene, camera, renderer, controls, stats, raycaster, projector;
var mouse = new THREE.Vector2(), INTERSECTED;
var keyboard = new THREEx.KeyboardState();
var clock = new THREE.Clock();
// custom global variables
var group;
var boundary;
var modelBoundingBox;
var sprite1;
var canvas1, context1, texture1;


init();
animate();

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}



function onDocumentMouseUp(event) {
    event.preventDefault();
    var mouse = new THREE.Vector2();
    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = -( event.clientY / window.innerHeight ) * 2 + 1;

    var mouseVector = new THREE.Vector3(
        ( event.clientX / window.innerWidth ) * 2 - 1,
        -( event.clientY / window.innerHeight ) * 2 + 1,
        0.5);
    mouseVector.unproject(camera);

    //projector.unprojectVector( mouseVector, camera );
    //var raycaster = new THREE.Raycaster(camera.position, mouseVector.sub(camera.position).normalize());

    raycaster.setFromCamera(mouse, camera);

    var intersects = raycaster.intersectObjects(scene.children, true);
    console.log(intersects);
    if (intersects.length > 0) {
        console.log("Intersected object:", intersects.length);
        intersects[0].object.material.color.setHex(Math.random() * 0xffffff);
    }

    /*
     raycaster.setFromCamera(mouse, camera);
     var intersects = raycaster.intersectObjects(scene.children, true);
     if (intersects.length > 0) {

     if ((true) || (INTERSECTED != intersects[0].object)) {


     //if (INTERSECTED) INTERSECTED.material.color.setHex(INTERSECTED.currentHex);
     INTERSECTED = intersects[0].object;

     console.log("Intersection found! " + INTERSECTED.name);
     //INTERSECTED.currentHex = INTERSECTED.material.color.getHex();
     //INTERSECTED.material.color.setHex(0xffff00);
     //INTERSECTED.visible = false;
     }
     } else {
     //   if (INTERSECTED) INTERSECTED.material.color.setHex(INTERSECTED.currentHex);
     INTERSECTED = null;
     }
     */
}

function onDocumentMouseMove(event) {
    event.preventDefault();
    // update sprite position
    //   sprite1.position.set(event.clientX, event.clientY - 20, 0);

    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = -( event.clientY / window.innerHeight ) * 2 + 1;


}

// FUNCTIONS
function init() {
    // SCENE
    scene = new THREE.Scene();
    // CAMERA
    var SCREEN_WIDTH = window.innerWidth, SCREEN_HEIGHT = window.innerHeight;
    var VIEW_ANGLE = 70, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;
    camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);

    camera.up = new THREE.Vector3(0, 0, 1);
    camera.lookAt(scene.position);

    scene.add(camera);
    camera.up.set(0, 0, 1);
    camera.position.set(100, 100, 100);
    //  camera.position.set(155739, 1218793, 469.05999755859375);

    projector = new THREE.Projector();
    raycaster = new THREE.Raycaster();
    document.addEventListener('mousemove', onDocumentMouseMove, false);
    document.addEventListener('mouseup', onDocumentMouseUp, false);
    window.addEventListener('resize', onWindowResize, false);

    // RENDERER
    if (Detector.webgl)
        renderer = new THREE.WebGLRenderer({antialias: true});
    else
        renderer = new THREE.CanvasRenderer();

    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    renderer.setClearColor(0x000000);
    renderer.sortObjects = false;

    container = document.getElementById('ThreeJS');
    container.appendChild(renderer.domElement);


    // EVENTS
    THREEx.WindowResize(renderer, camera);
    THREEx.FullScreen.bindKey({charCode: 'm'.charCodeAt(0)});
    // CONTROLS
    controls = new THREE.TrackballControls(camera, renderer.domElement);
    controls.rotateSpeed = 3.0;
    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;


    // STATS
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.bottom = '0px';
    stats.domElement.style.zIndex = 100;
    container.appendChild(stats.domElement);
    // LIGHT
    var light = new THREE.AmbientLight(0xffffff);
    light.position.set(0, 250, 0);
    scene.add(light);


    // FLOOR
    /*
     var floorTexture = new THREE.ImageUtils.loadTexture('images/checkerboard.jpg');
     floorTexture.wrapS = floorTexture.wrapT = THREE.RepeatWrapping;
     floorTexture.repeat.set(10, 10);
     var floorMaterial = new THREE.MeshBasicMaterial({map: floorTexture, side: THREE.DoubleSide});
     var floorGeometry = new THREE.PlaneGeometry(1000, 1000, 10, 10);
     var floor = new THREE.Mesh(floorGeometry, floorMaterial);
     floor.position.y = -0.5;
     floor.rotation.x = Math.PI / 2;
     scene.add(floor);
     */
    // SKYBOX/FOG
    /*    var skyBoxGeometry = new THREE.CubeGeometry(100000, 100000, 100000);
     var skyBoxMaterial = new THREE.MeshBasicMaterial({color: 0x9999ff, side: THREE.BackSide});
     var skyBox = new THREE.Mesh(skyBoxGeometry, skyBoxMaterial);
     skyBox.flipSided = true; // render faces from inside of the cube, instead of from outside (default).
     scene.add(skyBox);

     scene.fog = new THREE.FogExp2(0x9999ff, 0.00025);
     */


    ////////////
    // CUSTOM //
    ////////////
    // Sphere parameters: radius, segments along width, segments along height
    var sphereGeom = new THREE.SphereGeometry(50, 32, 16);

    // Basic wireframe materials.
    var darkMaterial = new THREE.MeshBasicMaterial({color: 0x000088});
    darkMaterial.side = THREE.DoubleSide;

    var darkMaterial2 = new THREE.MeshBasicMaterial({color: 0xFF0088});
    darkMaterial2.side = THREE.DoubleSide;
    //   darkMaterial.colors = THREE.FaceColors;

    var wireframeMaterial = new THREE.MeshBasicMaterial({color: 0x00ee00, wireframe: true, transparent: true});
    var multiMaterial = [darkMaterial, wireframeMaterial];
    var pong = new THREE.MeshPhongMaterial({color: 0x000088});
    pong.side = THREE.DoubleSide;
    var multiMaterial2 = [pong];


    // Creating three spheres to illustrate wireframes.
    /*
     var sphere = new THREE.Mesh(sphereGeom.clone(), new THREE.MeshBasicMaterial({color: 0xff0000}));
     sphere.position.set(0, 0, 0);
     sphere.scale.set(0.1, 0.1, 0.1);
     scene.add(sphere);
     */

    // оси
    var axes = new THREE.AxisHelper(200);
    scene.add(axes);


    var x0 = 0;
    var y0 = 0;
    var z0 = 0;


    group = new THREE.Object3D();

    scene.add(group);


    /*
     // cylinder
     var shape = THREE.SceneUtils.createMultiMaterialObject(
     // radiusAtTop, radiusAtBottom, height, segmentsAroundRadius, segmentsAlongHeight,
     new THREE.CylinderGeometry( 30, 30, 80, 20, 4 ),
     multiMaterial );
     shape.position.set(-200, 50, -100);
     scene.add( shape );
     * */

    var n = 3;
    for (var i = 2; i < (n + 1); i++) {
        loadModel("../obj/Test" + i, function (mesh) {
            group.add(mesh);

            if (i == (n + 1)) {
                getSizeModel();
                camera.position.set(modelBoundingBox.min.x + (boundary.size.x / 2),
                    modelBoundingBox.min.y + boundary.size.y + 200,
                    modelBoundingBox.min.z + (boundary.size.z / 2));
                light.position.set(modelBoundingBox.min.x + (boundary.size.x / 2),
                    modelBoundingBox.min.y + boundary.size.y + 400,
                    modelBoundingBox.min.z + (boundary.size.z / 2));

                controls.target = new THREE.Vector3(modelBoundingBox.min.x + (boundary.size.x / 2),
                    modelBoundingBox.min.y + (boundary.size.y / 2),
                    modelBoundingBox.min.z + (boundary.size.z / 2));

                //camera.up.set(0, 0, 1);

                //      camera.lookAt(mesh);
                //    scene.add(mesh);

                axes.position.set(controls.target);
                //  camera.up.set(0, 0, 0);

                loadHoles();
                INTERSECTED = null;
            }

        });
    }

    THREEx.WindowResize(renderer, camera);
    //   camera.lookAt(new THREE.Vector3( 150, 15, 30 ));
}

var getSizeModel = function () {
    // вычисляем размер 3d-объекта
    boundary = getBoundaryGeometry(group);
    //alert.text('Размеры 3d-объекта: ' + boundary.size.x + ', ' + boundary.size.y + ', ' + boundary.size.z);
};

var getBoundaryGeometry = function (obj) {

    modelBoundingBox = new THREE.Box3().setFromObject(obj);
    modelBoundingBox.size = {};
    modelBoundingBox.size.x = modelBoundingBox.max.x - modelBoundingBox.min.x;
    modelBoundingBox.size.y = modelBoundingBox.max.y - modelBoundingBox.min.y;
    modelBoundingBox.size.z = modelBoundingBox.max.z - modelBoundingBox.min.z;
    return modelBoundingBox;
};


function animate() {

    requestAnimationFrame(animate);
    render();
    update();

}

function update() {
    controls.update();
    stats.update();
}

function render() {


    renderer.render(scene, camera);

}
// THREE.OrbitControls = function ( object, domElement ) {

function loadModel(fileName, cb) {
    var darkMaterial = new THREE.MeshBasicMaterial({color: 0xff0000});
    darkMaterial.side = THREE.DoubleSide;
    var wireframeMaterial = new THREE.MeshBasicMaterial({color: 0x00ee00, wireframe: true, transparent: false});

    var pong = new THREE.MeshPhongMaterial({color: 0x000088});
    pong.side = THREE.DoubleSide;
    var multiMaterial2 = [darkMaterial, wireframeMaterial];


    var mtlLoader = new THREE.MTLLoader();
    mtlLoader.setBaseUrl('../');
    mtlLoader.load(fileName + '.mtl', function (materials) {
        materials.preload();
        for (var m in materials.materials) {
            //      materials.materials[m].side = THREE.DoubleSide;
            //   materials.materials[m].wireframe = true;
            //    materials.materials[m].wireframeLinewidth = 1;
            materials.materials = darkMaterial;
        }

        var oLoader = new THREE.OBJLoader();
        oLoader.setMaterials(materials);
        oLoader.load(fileName + '.obj', function (object) {
            object.position.x = 0;
            object.position.y = 0;
            object.position.z = 0;
            //  object.scale.set(0.1, 0.1, 0.1);
            object.children[0].material.wireframe = false;
            object.children[0].material.side = THREE.DoubleSide;

            cb(object);
        });
    });


}


function loadHoles() {
    for (var h in holesInfo.holes) {
        var material = new THREE.MeshLambertMaterial({color: 0xff0000});
        var geometry = new THREE.CylinderGeometry(1, 1, 100, 20, 4);
        var shape = new THREE.Mesh(
            // radiusAtTop, radiusAtBottom, height, segmentsAroundRadius, segmentsAlongHeight,
            geometry,
            material);


        shape.position.set(holesInfo.holes[h].x, holesInfo.holes[h].y, holesInfo.holes[h].z);
        shape.name = holesInfo.holes[h].name;
        //   shape.scale.set(0.1, 0.1, 0.1);
        shape.rotation.set(90 * Math.PI / 180, 0, 0);
        scene.add(shape);

        camera.position.set(shape.position.x, shape.position.y + 300, shape.position.z);
        camera.lookAt(shape.position);


    }
}

