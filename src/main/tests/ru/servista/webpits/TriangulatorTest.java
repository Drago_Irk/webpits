package ru.servista.webpits;

import com.vividsolutions.jts.geom.Coordinate;
import org.junit.*;

import java.util.ArrayList;

import static org.junit.Assert.*;


public class TriangulatorTest {

    @org.junit.Test
    public void testTriangulate() throws Exception {
        ArrayList<Coordinate> points = new ArrayList<>(4);
        ArrayList<Triangle> triangles;

        points.add(0, new Coordinate(0, 0, 0));
        points.add(1, new Coordinate(10, 0, 0));
        points.add(2, new Coordinate(10, 10, 0));
        points.add(3, new Coordinate(0, 10, 0));

        points.add(4, new Coordinate(20, 0, 0));
        points.add(5, new Coordinate(20, 10, 0));
   //     points.add(6, new Point(20, 10, 0));
        //points.add(4, new Point(0, 0, 0));


        triangles = Triangulator.triangulate(points).triangles;
        assert triangles.size() == 4;
    }
}