package ru.servista.webpits;

/**
 * Created by drago on 20/03/16.
 */
public class LytographyTest {

    @org.junit.Test
    public void testHasSolidLayer() {
        Lytography lyt = new Lytography();
        Solid solid;

        solid = new Solid();
        solid.name = "1";
        solid.depth = 10;
        lyt.addSolid(solid);

        solid = new Solid();
        solid.name = "2";
        solid.depth = 20;
        lyt.addSolid(solid);

        solid = new Solid();
        solid.name = "3";
        solid.depth = 40;
        lyt.addSolid(solid);

        solid = new Solid();
        solid.name = "4";
        solid.depth = 50;
        lyt.addSolid(solid);

        solid = new Solid();
        solid.name = "5";
        solid.depth = 55;
        lyt.addSolid(solid);

        solid = new Solid();
        solid.name = "7";
        solid.depth = 90;
        lyt.addSolid(solid);

        assert lyt.hasSolidLayer("1", 0, 5);
        assert lyt.hasSolidLayer("2", 15, 25);
        assert lyt.hasSolidLayer("7", 85, 100);

        assert !lyt.hasSolidLayer("12", 85, 100);
        assert !lyt.hasSolidLayer("1", 11, 20);
        assert !lyt.hasSolidLayer("2", 0, 9);
    }

}
