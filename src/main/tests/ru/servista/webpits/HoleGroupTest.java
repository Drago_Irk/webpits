package ru.servista.webpits;

import com.vividsolutions.jts.geom.Coordinate;
import org.junit.Ignore;

import java.util.List;
import java.util.Set;

/**
 * Created by drago on 20/03/16.
 */
public class HoleGroupTest {

    @org.junit.Test
    public void testHolesLoad() {
        HoleGroup pit = new HoleGroup();
        try {
            pit.loadHolesFromTXTFile("testData/Наб.txt");
            Hole[] arr = pit.getHolesAsArray();
            assert arr.length == 12;

        } catch (Exception e) {
            assert false;
        }

        try {
            pit.loadLytographyFromTXTFile("testData/Песок.txt");
            pit.loadLytographyFromTXTFile("testData/Песчаник.txt");
            pit.loadLytographyFromTXTFile("testData/Щебень.txt");

        } catch (Exception e) {
            assert false;
        }
        pit.triangulateHoles();
        pit.makeLayers();
        pit.make3dModel();
        assert true;

    }

    @Ignore
    @org.junit.Test
    public void testGetNeighborsForHole() {
        HoleGroup pit = new HoleGroup();
        Hole hole1, hole2, hole5;
        hole1 = new Hole("1", new Coordinate(0, 0, 0));
        hole1.getLyt().addSolid(new Solid("Песок", -10));
        hole1.getLyt().addSolid(new Solid("Вода", -50));
        hole1.getLyt().addSolid(new Solid("Камень", -100));
        pit.addHole(hole1);

        hole2 = new Hole("2", new Coordinate(10, 0, 0));
        hole2.getLyt().addSolid(new Solid("Песок", -20));
        hole2.getLyt().addSolid(new Solid("Вода", -80));
        hole2.getLyt().addSolid(new Solid("Камень", -120));

        pit.addHole(hole2);

        pit.addHole(new Hole("3", new Coordinate(20, 0)));
        pit.addHole(new Hole("4", new Coordinate(0, 10)));
        hole5 = new Hole("5", new Coordinate(10, 10));
        pit.addHole(hole5);
        pit.addHole(new Hole("6", new Coordinate(20, 10)));
        pit.addHole(new Hole("7", new Coordinate(0, 20)));
        pit.addHole(new Hole("8", new Coordinate(10, 20)));
        pit.addHole(new Hole("9", new Coordinate(20, 20)));
        pit.triangulateHoles();

        Set<Hole> n1 =  pit.getNeighborsForHole(hole1);
        Set<Hole> n5 =  pit.getNeighborsForHole(hole5);
        Set<Hole> nx =  pit.getNeighborsForHole(new Hole("x", new Coordinate(120, 110)));

        assert n1.size() == 2;
        assert n5.size() == 6;
        assert nx.isEmpty();
    }

    @Ignore
    @org.junit.Test
    public void testSimplestPit() {
        HoleGroup pit = new HoleGroup();
        Hole hole0, hole1, hole2, hole3, hole4;

        hole0 = new Hole("0", new Coordinate(0, 0, 1050));
        hole0.getLyt().addSolid(new Solid("Песок", 900));
        hole0.getLyt().addSolid(new Solid("Вода", 600));
        pit.addHole(hole0);

        hole1 = new Hole("1", new Coordinate(100, 0, 1100));
        hole1.getLyt().addSolid(new Solid("Песок", 900));
        hole1.getLyt().addSolid(new Solid("Вода", 400));
        pit.addHole(hole1);

        hole2 = new Hole("2", new Coordinate(100, 100, 1020));
        hole2.getLyt().addSolid(new Solid("Песок", 900));
        hole2.getLyt().addSolid(new Solid("Вода", 300));
        pit.addHole(hole2);


        hole3 = new Hole("3", new Coordinate(0, 100, 1070));
        hole3.getLyt().addSolid(new Solid("Песок", 900));
        hole3.getLyt().addSolid(new Solid("Вода", 459));
        pit.addHole(hole3);



        pit.triangulateHoles();

        pit.makeLayers();
        pit.make3dModel();

        assert pit.getLayers().size() == 2;
    }
}
