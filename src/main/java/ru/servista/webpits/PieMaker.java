package ru.servista.webpits;

import com.vividsolutions.jts.geom.Coordinate;
import eu.mihosoft.vrl.v3d.CSG;
import eu.mihosoft.vrl.v3d.ObjFile;
import javafx.scene.shape.MeshView;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;


/**
 * Created by user on 21.02.2016.
 */
public class PieMaker {
    ArrayList<Coordinate> holes = new ArrayList<>();
    ArrayList<Triangle> surfaceTriangles = new ArrayList<>();
    ArrayList<SideInfo> perimetrPoints;

    public PieMaker() {
        init();
    }

    private void init() {
        holes.add(new Coordinate(0, 0, 0));
        holes.add(new Coordinate(100, 0, 0));
        holes.add(new Coordinate(200, 0, 0));
        holes.add(new Coordinate(300, 0, 0));

        holes.add(new Coordinate(0, 10, 0));
        holes.add(new Coordinate(100, 10, 0));
        holes.add(new Coordinate(200, 10, 0));
        holes.add(new Coordinate(300, 10, 0));

        holes.add(new Coordinate(0, 20, 0));
        holes.add(new Coordinate(100, 20, 0));
        holes.add(new Coordinate(200, 20, 0));
        holes.add(new Coordinate(300, 20, 0));

        holes.add(new Coordinate(0, 30, 0));
        holes.add(new Coordinate(100, 30, 0));
        holes.add(new Coordinate(200, 30, 0));
        holes.add(new Coordinate(300, 30, 0));

        holes.add(new Coordinate(0, 30, 0));
        holes.add(new Coordinate(100, 30, 0));
        holes.add(new Coordinate(200, 30, 0));
        holes.add(new Coordinate(300, 30, 0));

        holes.add(new Coordinate(0, 40, 0));
        holes.add(new Coordinate(100, 40, 0));
        holes.add(new Coordinate(200, 40, 0));
        holes.add(new Coordinate(300, 40, 0));

        holes.add(new Coordinate(0, 50, 0));
        holes.add(new Coordinate(100, 50, 0));
        holes.add(new Coordinate(200, 50, 0));
        holes.add(new Coordinate(300, 50, 0));

        holes.add(new Coordinate(0, 60, 0));
        holes.add(new Coordinate(100, 60, 0));
        holes.add(new Coordinate(200, 60, 0));
        holes.add(new Coordinate(300, 60, 0));

        holes.add(new Coordinate(0, 70, 0));
        holes.add(new Coordinate(100, 70, 0));
        holes.add(new Coordinate(200, 70, 0));
        holes.add(new Coordinate(300, 70, 0));

        holes.add(new Coordinate(0, 80, 0));
        holes.add(new Coordinate(100, 80, 0));
        holes.add(new Coordinate(200, 80, 0));
        holes.add(new Coordinate(300, 80, 0));

        holes.add(new Coordinate(0, 90, 0));
        holes.add(new Coordinate(100, 90, 0));
        holes.add(new Coordinate(200, 90, 0));
        holes.add(new Coordinate(300, 90, 0));

        holes.add(new Coordinate(0, 140, 0));
        holes.add(new Coordinate(100, 140, 0));
        holes.add(new Coordinate(200, 140, 0));
        holes.add(new Coordinate(300, 140, 0));

        holes.add(new Coordinate(0, 190, 0));
        holes.add(new Coordinate(100, 190, 0));
        holes.add(new Coordinate(200, 190, 0));
        holes.add(new Coordinate(300, 190, 0));

        holes.add(new Coordinate(0, 240, 0));
        holes.add(new Coordinate(100, 240, 0));
        holes.add(new Coordinate(200, 240, 0));
        holes.add(new Coordinate(300, 240, 0));

        try {
            TriangulationResult res = Triangulator.triangulate(holes);
            surfaceTriangles = res.triangles;
            perimetrPoints = res.perimetrPoints;

            //System.out.println(surfaceTriangles.size());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void saveToFile(CSG mesh, String fileName) {
        ObjFile f = mesh.toObj();
        System.out.println(f.getObj());
        Path p = Paths.get(fileName);
        try {
            f.toFiles(p);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void makePie() {
        PieLayer layer = null;
        for (int i = 0; i < 10; i++) {
            layer = new PieLayer(layer == null ? holes : layer.getBottomPoints(), surfaceTriangles, perimetrPoints, Math.random() * 50);
            CSG csgMesh = Mesh2CSG.mesh2CSG(layer.getTriangleMesh(), false);
            saveToFile(csgMesh, String.format("./obj/Test%s.obj", i));
        }
    }

    public static void main(String[] args) {
        PieMaker pieMaker = new PieMaker();
        pieMaker.makePie();
    }
}
