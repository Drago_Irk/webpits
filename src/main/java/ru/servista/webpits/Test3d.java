package ru.servista.webpits;

import eu.mihosoft.vrl.v3d.CSG;
import eu.mihosoft.vrl.v3d.Cube;
import eu.mihosoft.vrl.v3d.ObjFile;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javafx.scene.shape.TriangleMesh;
import ru.servista.webpits.Mesh2CSG;

/**
 * Created by drago on 07/02/16.
 */
public class Test3d {

    public static void main(String[] args) {
//        Image dieImage = new Image(getClass().getResourceAsStream("images/die.gif"));

//        PhongMaterial material = new PhongMaterial();
//        material.setDiffuseMap(dieImage);
//        material.setSpecularColor(Color.WHITE);
/*
        float hw = 100/2f;
        float hh = 100/2f;
        float hd = 100/2f;

        float points[] = {
                hw, hh, hd,
                hw, hh, -hd,
                hw, -hh, hd,
                hw, -hh, -hd,
                -hw, hh, hd,
                -hw, hh, -hd,
                -hw, -hh, hd,
                -hw, -hh, -hd};

        float tex[] = {
                100, 0,
                200, 0,
                0, 100,
                100, 100,
                200, 100,
                300, 100,
                400, 100,
                0, 200,
                100, 200,
                200, 200,
                300, 200,
                400, 200,
                100, 300,
                200, 300};

        int faces[] = {
                0, 10, 2, 5, 1, 9,
                2, 5, 3, 4, 1, 9,
                4, 7, 5, 8, 6, 2,
                6, 2, 5, 8, 7, 3,
                0, 13, 1, 9, 4, 12,
                4, 12, 1, 9, 5, 8,
                2, 1, 6, 0, 3, 4,
                3, 4, 6, 0, 7, 3,
                0, 10, 4, 11, 2, 5,
                2, 5, 4, 11, 6, 6,
                1, 9, 3, 4, 5, 8,
                5, 8, 3, 4, 7, 3};

        TriangleMesh mesh = new TriangleMesh();
        mesh.getPoints().addAll(points);
        mesh.getTexCoords().addAll(tex);
        mesh.getFaces().addAll(faces);
        CSG csgMesh = Mesh2CSG.mesh2CSG(mesh);

//        CSG cube = new Cube(10, 10, 10).toCSG();
        ObjFile f = csgMesh.toObj();
        System.out.println(f.getObj());
        Path p = Paths.get("./Test.obj");
        try {
            f.toFiles(p);
        } catch (IOException e) {
            e.printStackTrace();
        } */









        float hw = 100/2f;
        float hh = 100/2f;
        float hd = 100/2f;

        float points[] = {
                hw, hh, hd,
                hw, hh, -hd,
                hw, -hh, hd,
                hw, -hh, -hd,
                -hw, hh, hd,
                -hw, hh, -hd,
                -hw, -hh, hd,
                -hw, -hh, -hd};

        float tex[] = {
                100, 0,
                200, 0,
                0, 100,
                100, 100,
                200, 100,
                300, 100,
                400, 100,
                0, 200,
                100, 200,
                200, 200,
                300, 200,
                400, 200,
                100, 300,
                200, 300};

        int faces[] = {
                0, 10, 2, 5, 1, 9,
                2, 5, 3, 4, 1, 9,
                4, 7, 5, 8, 6, 2,
                6, 2, 5, 8, 7, 3,
                0, 13, 1, 9, 4, 12,
                4, 12, 1, 9, 5, 8,
                2, 1, 6, 0, 3, 4,
                3, 4, 6, 0, 7, 3,
                0, 10, 4, 11, 2, 5,
                2, 5, 4, 11, 6, 6,
                1, 9, 3, 4, 5, 8,
                5, 8, 3, 4, 7, 3};

        TriangleMesh mesh = new TriangleMesh();

        mesh.getPoints().addAll(points);
        mesh.getTexCoords().addAll(tex);
        mesh.getFaces().addAll(faces);


        CSG csgMesh = Mesh2CSG.mesh2CSG(mesh, false);

//        CSG cube = new Cube(10, 10, 10).toCSG();
        ObjFile f = csgMesh.toObj();
        System.out.println(f.getObj());
        Path p = Paths.get("./Test.obj");
        try {
            f.toFiles(p);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
