package ru.servista.webpits;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * Created by drago on 13/02/16.
 */
public class Hole {
    private String name;
    private Coordinate coords;
    private Lytography lyt = new Lytography();

    public Hole() {

    }

    public Hole(String name, Coordinate coord) {
        this.setName(name);
        this.setCoords(coord);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coordinate getCoords() {
        return coords;
    }

    public void setCoords(Coordinate coords) {
        this.coords = coords;
    }

    public void addSolid(Solid solid) {
        lyt.addSolid(solid);
    }

    public Lytography getLyt() {
        return lyt;
    }

    public boolean hasSolidLayer(String name, double top, double bottom) {
        return lyt.hasSolidLayer(name, top, bottom);
    }

    public Solid getUnusedSolidLayerByInterval(String name, double top, double bottom) {
        return lyt.getUnusedSolidLayerByInterval(name, top, bottom);
    }

    public Solid getAnySolidLayerByInterval(double top, double bottom) {
        return lyt.getAnySolidLayerByInterval(top, bottom);
    }



    public double getTopForSolid(Solid solid) throws Exception{
        double top;
        if(solid.isVirtual) {
            top = solid.depth;
        } else top = lyt.getTopForSolid(solid);

        if (top == 0) {
            top = this.getCoords().z;
        }

        return top;
    }

}
