package ru.servista.webpits;

import java.util.*;

/**
 * Created by drago on 19/03/16.
 */

class SortByDepth implements Comparator<Solid> {
    public int compare(Solid a, Solid b) {
        return (a.depth < b.depth) ? 1 : -1;
    }
}


public class Lytography {
    public Set<Solid> solids = new TreeSet<>(new SortByDepth());

    public void addSolid(Solid solid) {
        solids.add(solid);
    }

    public Solid[] getSolids() {
        return solids.toArray(new Solid[solids.size()]);
    }

    public boolean hasSolidLayer(String name, double top, double bottom) {
        boolean res = false;
        Iterator<Solid> iterator = solids.iterator();
        double previousDepth = 0;

        while (iterator.hasNext()) {
            Solid solid = iterator.next();
            if (!solid.name.equals(name)) {
                previousDepth = solid.depth;
                continue;
            }

            if (((solid.depth <= bottom) && (solid.depth >= top) || ((previousDepth <= bottom) && (previousDepth >= top)))) {
                res = true;
                break;
            }
            previousDepth = solid.depth;
        }
        return res;
    }


    public Solid getUnusedSolidLayerByInterval(String name, double top, double bottom) {
        Solid res = null;
        Solid[] solids = getSolidsAsArray();
        double previousDepth = 0;
        for (Solid solid : solids) {
            if (!solid.name.equals(name)) {
                previousDepth = solid.depth;
                continue;
            }

            if ((solid.tag == 0) && ((solid.depth <= bottom) && (previousDepth >= bottom) ||
                    ((solid.depth >= bottom) && (solid.depth <= top)))) {
                res = solid;
                break;
            }

            /*
             if ((solid.tag == 0) && ((solid.depth <= bottom) && (solid.depth >= top) ||
                    ((previousDepth <= bottom) && (previousDepth >= top)))) {
                res = solid;
                break;
            }
            * */
            previousDepth = solid.depth;
        }
        return res;
    }

    public Solid getAnySolidLayerByInterval(double top, double bottom) {
        Solid res = null;
        Solid[] solids = getSolidsAsArray();
        double previousDepth = 0;
        for (Solid solid : solids) {
            if ((solid.depth <= bottom) && (previousDepth >= bottom) ||
                    ((solid.depth >= bottom) && (solid.depth <= top))) {
                res = solid;
                break;
            }
            previousDepth = solid.depth;
        }
        return res;
    }


    public Solid[] getSolidsAsArray() {
        Solid[] res = new Solid[solids.size()];
        solids.toArray(res);
        return res;
    }

    public double getTopForSolid(Solid solid) throws Exception {
        double result = -1;
        Solid[] solids = getSolidsAsArray();
        for (int i = 0; i < solids.length; i++) {
            if (solids[i].equals(solid)) {
                result = (i == 0) ? 0 : solids[i - 1].depth;
                break;
            }
        }
        if (result == -1) {
            throw new Exception("Нет такой породы! " + solid);
        }
        return result;
    }


}