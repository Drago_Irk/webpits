package ru.servista.webpits;

import com.vividsolutions.jts.geom.Coordinate;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.shape.MeshView;

import javafx.scene.paint.Color;
import java.util.*;

/**
 * Created by drago on 28/02/16.
 */
public class PieLayer {
    private ArrayList<Coordinate> coordsTop;
    private ArrayList<Coordinate> coordsBottom;
    private ArrayList<Triangle> top;
    private ArrayList<Triangle> bottom;
    private ArrayList<Triangle> sides;
    private ArrayList<SideInfo> perimetrPoints;

    public ArrayList<Coordinate> getBottomPoints() {
        return coordsBottom;
    }


    public PieLayer(ArrayList<Coordinate> layerTop, ArrayList<Triangle> surfaceTriangles, ArrayList<SideInfo> perimetrPoints,
                    double height) {
        this.perimetrPoints = perimetrPoints;
        coordsTop = new ArrayList<>(layerTop.size());
        coordsBottom = new ArrayList<>(layerTop.size());

        for (Coordinate c : layerTop) {
            coordsTop.add((Coordinate) c.clone());
            coordsBottom.add((Coordinate) c.clone());
        }

        top = surfaceTriangles;
        bottom = surfaceTriangles;

        for (Coordinate c : coordsTop) {
            c.z = c.z + 0;
        }
        for (Coordinate c : coordsBottom) {
            c.z = c.z + height + Math.random()*10;
        }

    }

    public TriangleMesh getTriangleMesh() {
        float[] pointsArray = new float[coordsTop.size() * 3 + coordsBottom.size() * 3];
        int faces[] = new int[top.size() * 6 + bottom.size() * 6 + perimetrPoints.size() * 2 * 6];
        float tex[] = {0};

        int idx = 0;
        for (Coordinate c : coordsTop) {
            pointsArray[idx] = (float) c.x;
            idx++;
            pointsArray[idx] = (float) c.y;
            idx++;
            pointsArray[idx] = (float) c.z;
            idx++;
        }
        for (Coordinate c : coordsBottom) {
            pointsArray[idx] = (float) c.x;
            idx++;
            pointsArray[idx] = (float) c.y;
            idx++;
            pointsArray[idx] = (float) c.z;
            idx++;
        }

        idx = 0;
        for (Triangle tr : top) {
            faces[idx] = tr.p[0];
            idx++;
            faces[idx] = 0;
            idx++;
            faces[idx] = tr.p[1];
            idx++;
            faces[idx] = 0;
            idx++;
            faces[idx] = tr.p[2];
            idx++;
            faces[idx] = 0;
            idx++;
        }
        for (Triangle tr : bottom) {
            faces[idx] = tr.p[0] + coordsTop.size();
            idx++;
            faces[idx] = 0;
            idx++;
            faces[idx] = tr.p[1] + coordsTop.size();
            idx++;
            faces[idx] = 0;
            idx++;
            faces[idx] = tr.p[2] + coordsTop.size();
            idx++;
            faces[idx] = 0;
            idx++;
        }

        for (SideInfo info : perimetrPoints) {
            // первый боковой треугольник
            faces[idx] = info.pointIdx1;
            idx++;
            faces[idx] = 0;
            idx++;

            faces[idx] = info.pointIdx1 + coordsTop.size();
            idx++;
            faces[idx] = 0;
            idx++;

            faces[idx] = info.pointIdx2;
            idx++;
            faces[idx] = 0;
            idx++;

            // второй боковой треугольник
            faces[idx] = info.pointIdx2;
            idx++;
            faces[idx] = 0;
            idx++;


            faces[idx] = info.pointIdx1 + coordsTop.size();
            idx++;
            faces[idx] = 0;
            idx++;

            faces[idx] = info.pointIdx2 + coordsTop.size();
            idx++;
            faces[idx] = 0;
            idx++;
        }



        PhongMaterial material = new PhongMaterial();
        material.setSpecularColor(new Color(Math.random(), Math.random(), Math.random(), 1));
        material.setDiffuseColor(new Color(Math.random(), Math.random(), Math.random(), 1));



        TriangleMesh mesh = new TriangleMesh();
        mesh.getPoints().addAll(pointsArray);
        mesh.getTexCoords().addAll(tex);
        mesh.getFaces().addAll(faces);

        return mesh;
    }


}

