package ru.servista.webpits;

import eu.mihosoft.vrl.v3d.CSG;
import eu.mihosoft.vrl.v3d.Cube;
import eu.mihosoft.vrl.v3d.ObjFile;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javafx.scene.paint.Color;
import javafx.scene.shape.TriangleMesh;
import ru.servista.webpits.Mesh2CSG;

/**
 * Created by drago on 07/02/16.
 */
public class Test3d2 {

    public static void main(String[] args) {
//        Image dieImage = new Image(getClass().getResourceAsStream("images/die.gif"));

//        PhongMaterial material = new PhongMaterial();
//        material.setDiffuseMap(dieImage);
 //       material.setSpecularColor(Color.WHITE);

        float hw = 100/2f;
        float hh = 100/2f;
        float hd = 100/2f;

        float points[] = {
                0, 0, 0,
                50, 0, 0,
                50, 50, 0,
                0, 50, 0};

        float tex[] = {0};

        int faces[] = {
                0, 0, 2, 0, 1, 0,
                0, 0, 3, 0, 2, 0};

        TriangleMesh mesh = new TriangleMesh();
        mesh.getPoints().addAll(points);
        mesh.getTexCoords().addAll(tex);
        mesh.getFaces().addAll(faces);
        CSG csgMesh = Mesh2CSG.mesh2CSG(mesh, false);

//        CSG cube = new Cube(10, 10, 10).toCSG();
        ObjFile f = csgMesh.toObj();
        System.out.println(f.getObj());
        Path p = Paths.get("./Test.obj");
        try {
            f.toFiles(p);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
