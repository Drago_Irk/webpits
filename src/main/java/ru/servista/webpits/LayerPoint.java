package ru.servista.webpits;

/**
 * Created by drago on 24/03/16.
 */
public class LayerPoint {
    public Hole hole;
    public Solid solid;

    public LayerPoint(Hole hole, Solid solid) {
        this.hole = hole;
        this.solid = solid;

    }

    public String toString() {
        String s = "Скважина: " + hole.getName() + ", " + solid.name + ", " + solid.depth;
        return s;
    }
}
