package ru.servista.webpits;

import com.vividsolutions.jts.geom.Coordinate;
import eu.mihosoft.vrl.v3d.CSG;
import eu.mihosoft.vrl.v3d.ObjFile;
import javafx.scene.paint.Color;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by drago on 19/03/16.
 */
public class HoleGroup {
    private Map<String, Hole> holes = new HashMap<>();
    private List<Coordinate> coords;
    private List<Triangle> surfaceTriangles = new ArrayList<>();
    private List<SideInfo> perimetrPoints = new ArrayList<>();
    private List<Layer> layers = new ArrayList<>();

    public List<Layer> getLayers() {
        return layers;
    }

    public void addHole(Hole hole) {
        holes.put(hole.getName(), hole);
    }

    public void addSolid(String holeName, Solid solid) throws Exception {
        Hole hole = getHoleByName(holeName);
        if (hole == null) {
            throw new Exception(String.format("Ошибка добавления породы в скважину %s. Неизвестная скважина!", holeName));
        }
        hole.addSolid(solid);
    }

    public Hole getHoleByName(String holeName) {
        return holes.get(holeName);
    }

    public void loadHolesFromTXTFile(String fileName) throws Exception {
        File file = new File(fileName);
        BufferedReader reader = null;
        FileInputStream fStream = null;
        InputStreamReader iReader = null;

        try {
            fStream = new FileInputStream(file);
            iReader = new InputStreamReader(fStream, "windows-1251");
            reader = new BufferedReader(iReader);
            String line;

            while ((line = reader.readLine()) != null) {
                String[] arr = line.split(",");
                if (arr.length != 4) {
                    throw new Exception(String.format("Ошибка в структуре файла %s, строка '%s'", fileName, line));
                }
                Hole h = new Hole();
                h.setName(arr[0]);
                h.setCoords(new Coordinate(Double.parseDouble(arr[1]), Double.parseDouble(arr[2]), Double.parseDouble(arr[3])));
                addHole(h);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadLytographyFromTXTFile(String fileName) throws Exception {
        File file = new File(fileName);
        BufferedReader reader = null;
        FileInputStream fStream = null;
        InputStreamReader iReader = null;

        try {
            fStream = new FileInputStream(file);
            iReader = new InputStreamReader(fStream, "windows-1251");
            reader = new BufferedReader(iReader);
            String line;

            while ((line = reader.readLine()) != null) {
                String[] arr = line.split(",");
                if (arr.length != 4) {
                    throw new Exception(String.format("Ошибка в структуре файла %s, строка '%s'", fileName, line));
                }

                String holeName = arr[0];
                Coordinate c = new Coordinate(Double.parseDouble(arr[1]), Double.parseDouble(arr[2]), Double.parseDouble(arr[3]));
                Solid solid = new Solid();
                solid.name = file.getName();
                while (solid.name.lastIndexOf(".") != -1) {
                    solid.name = solid.name.substring(0, solid.name.lastIndexOf("."));
                }
                solid.depth = c.z;

                this.addSolid(holeName, solid);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Hole[] getHolesAsArray() {
        Hole[] res = new Hole[holes.size()];
        holes.values().toArray(res);
        return res;
    }

    public void triangulateHoles() {
        try {
            coords = new ArrayList<>(holes.size());
            Hole[] holes = getHolesAsArray();
            for (Hole hole : holes) {
                coords.add(hole.getCoords());
            }

            TriangulationResult res = Triangulator.triangulate(coords);
            surfaceTriangles = res.triangles;
            perimetrPoints = res.perimetrPoints;

            System.out.println(surfaceTriangles.size());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void saveToFile(CSG mesh, String fileName) {
        ObjFile f = mesh.toObj();
        System.out.println(f.getObj());
        Path p = Paths.get(fileName);
        try {
            f.toFiles(p);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Set<Hole> getNeighborsForHole(Hole hole) {
        Set<Hole> result = new HashSet<>();

        Hole[] holes = getHolesAsArray();
        for (Triangle tr : surfaceTriangles) {
            for (int i = 0; i < 3; i++) {
                if (holes[tr.p[i]].equals(hole)) {
                    for (int j = 0; j < 3; j++) {
                        if (j != i) {
                            result.add(holes[tr.p[j]]);
                        }
                    }
                    break;
                }
            }
        }
        return result;
    }


    private void makeLayerForHoleAndSolid(Solid solid, Hole hole, Layer curLayer) {
        if (curLayer.getPoints().size() == 0) {
            curLayer.addPoint(new LayerPoint(hole, solid));
            solid.tag = 1;
        }

        double top;
        Set<Hole> neighbors = getNeighborsForHole(hole);
        try {
            top = hole.getTopForSolid(solid);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        for (Hole neighbor : neighbors) {
            if (curLayer.contains(neighbor)) {
                continue;
            }
            Solid neighborSolid = neighbor.getUnusedSolidLayerByInterval(solid.name, top, solid.depth);
            if ((neighborSolid != null) && (neighborSolid.tag == 0)) {
                curLayer.addPoint(new LayerPoint(neighbor, neighborSolid));
                neighborSolid.tag = 1;
                makeLayerForHoleAndSolid(neighborSolid, neighbor, curLayer);
            } else if (neighborSolid == null) {
                neighborSolid = neighbor.getAnySolidLayerByInterval(top, solid.depth);
                if (neighborSolid != null) {
                    Solid virtualSolid = new Solid();
                    virtualSolid.tag = 1;
                    virtualSolid.name = "";
                    virtualSolid.depth = getDepthForVirtualSolid(hole, neighbor, solid);
                    //neighborSolid.depth - 0.00001D;
                    //
                    //
                    virtualSolid.isVirtual = true;
                    curLayer.addPoint(new LayerPoint(neighbor, virtualSolid));
                }
            }

        }
    }

    private double getDepthForVirtualSolid(Hole hole, Hole neighbor, Solid solid) {
        Solid[] solids1 = hole.getLyt().getSolidsAsArray();
        Solid[] solids2 = neighbor.getLyt().getSolidsAsArray();

        Set<Solid> sortedSolids = new TreeSet<>(new SortByDepth());
        for (Solid curSolid : solids1) {
            curSolid.mark = 1;
            sortedSolids.add(curSolid);
            if (curSolid.equals(solid)) {
                break;
            }
        }
        for (Solid curSolid : solids2) {
            curSolid.mark = 0;
            sortedSolids.add(curSolid);
        }

        int idx = 0;
        Solid[] sortedSolidsArray = new Solid[sortedSolids.size()];
        sortedSolids.toArray(sortedSolidsArray);

        boolean found = false;
        for (int i = 0; i < sortedSolidsArray.length; i++) {
            if (sortedSolidsArray[i].equals(solid)) {
                for (int j = i - 1; j >= 0; j--) {
                    if (sortedSolidsArray[j].mark == 0) {
                        found = true;
                        return sortedSolidsArray[j].depth;
                    }
                }
                // не нашли более ранних пород - значит, мы первые
                return neighbor.getCoords().z;
            }
        }
        return 0;
    }


    public void makeLayers() {
        layers.clear();

        Hole[] holes = getHolesAsArray();
        Hole hole;
        Solid solid;
        Lytography lyt;
        Solid[] solids;


        for (int i = 0; i < holes.length; i++) {
            hole = holes[i];
            lyt = hole.getLyt();
            solids = lyt.getSolidsAsArray();
            for (int j = 0; j < solids.length; j++) {
                solid = solids[j];
                if (solid.tag == 0) {
                    Layer layer = new Layer();
                    makeLayerForHoleAndSolid(solid, hole, layer);
                    if (!layer.isEmpty()) {
                        layers.add(layer);
                        System.out.println(layer);
                    }
                }
            }
        }

    }


    public void make3dModel() {
        int idx = 1;
        // 2 и 3 пересекаются
        for (Layer layer : layers) {
            if (layer.getPoints().size() < 3) {
                continue;
            }
            CSG csgMesh = Mesh2CSG.mesh2CSG(layer.getTriangleMesh(), true);
            csgMesh.color(new Color(Math.random(), Math.random(), Math.random(), 1));
            saveToFile(csgMesh, String.format("./obj/Test%s.obj", idx));
            idx++;
        }
        try {
            saveHolesToFile("./obj/holes.json");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void saveHolesToFile(String fileName) throws IOException {
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();
        JSONObject jsonHole;

        json.put("holes", arr);
        for (Hole h : holes.values()) {
            jsonHole = new JSONObject();
            jsonHole.put("name", h.getName());
            jsonHole.put("x", h.getCoords().x);
            jsonHole.put("y", h.getCoords().y);
            jsonHole.put("z", -h.getCoords().z);
            arr.add(jsonHole);
        }


        try (FileWriter fw = new FileWriter(fileName)) {
            json.writeJSONString(fw);
        }
        json.clear();
    }
}
