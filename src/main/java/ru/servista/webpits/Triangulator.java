package ru.servista.webpits;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.triangulate.DelaunayTriangulationBuilder;
import com.vividsolutions.jts.triangulate.quadedge.QuadEdge;
import com.vividsolutions.jts.triangulate.quadedge.QuadEdgeSubdivision;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by drago on 13/02/16.
 */
public class Triangulator {
    public static TriangulationResult triangulate(List<Coordinate> points) throws Exception {
        TriangulationResult result = new TriangulationResult();
        DelaunayTriangulationBuilder builder = new DelaunayTriangulationBuilder();
        builder.setSites(points);
        GeometryFactory geoFactory = new GeometryFactory();

        QuadEdgeSubdivision subdivision = builder.getSubdivision();
        Geometry g = subdivision.getTriangles(geoFactory);

        System.out.println("Точки периметра:");
        ArrayList<QuadEdge> edges =  (ArrayList<QuadEdge>)subdivision.getPrimaryEdges(false);
        for(QuadEdge q: edges) {
           if(subdivision.isFrameBorderEdge(q)) {
               Coordinate p0 = q.orig().getCoordinate();
               Coordinate p1 = q.dest().getCoordinate();
               int idx = points.indexOf(p0);
               int idx2 = points.indexOf(p1);
               result.perimetrPoints.add(new SideInfo(idx, idx2));
               System.out.println(String.format("%s - %s", idx, idx2));

           }
        }


        Coordinate[] triangles2 = g.getCoordinates();
        ArrayList<Coordinate> triangles3 = new ArrayList<>();
        for (int i = 0; i < triangles2.length; i++) {
            if (!(((i + 1) % 4) == 0)) {
                triangles3.add(triangles2[i]);
            }
        }

        Triangle tr = null;
        int pointIdx = 0;

        for (int i = 0; i < triangles3.size(); i++) {
            int idx = points.indexOf(triangles3.get(i));
            if (idx == -1) {
                throw new Exception("Точки не было в исходных координатах!");
            }
            if ((i + 1 / 3) % 3 == 0) {
                tr = new Triangle();
                result.triangles.add(tr);
                pointIdx = 0;
            }
            tr.p[pointIdx] = idx;
            pointIdx++;
        }

        System.out.println("Закончили триангуляцию");
        return result;
    }

}
