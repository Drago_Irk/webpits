package ru.servista.webpits;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by drago on 01/03/16.
 */
public class TriangulationResult {
    public ArrayList<Triangle> triangles = new ArrayList<>();
    public ArrayList<SideInfo> perimetrPoints = new ArrayList<>();
}
