package ru.servista.webpits;

/**
 * Created by drago on 19/03/16.
 */
public class Solid {
    public String name = "";
    public double depth;
    public int tag = 0;
    public int mark = 0;
    public boolean isVirtual = false;

    public Solid() {

    }

    public Solid(String name, double depth) {
        this.name = name;
        this.depth = depth;
    }


}
