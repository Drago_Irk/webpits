package ru.servista.webpits;

import com.vividsolutions.jts.geom.Coordinate;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.TriangleMesh;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by drago on 24/03/16.
 */
public class Layer {
    private List<LayerPoint> points = new ArrayList<>();
    private List<Coordinate> coordsTop = new ArrayList<>();
    private List<Coordinate> coordsBottom = new ArrayList<>();

    private List<Triangle> top;
    private List<Triangle> bottom;
    private List<Triangle> sides;
    private List<SideInfo> perimetrPoints;


    public void addPoint(LayerPoint point) {
        points.add(point);
    }

    public List<LayerPoint> getPoints() {
        return points;
    }

    public String toString() {
        String s = "Слой: \n";
        List<LayerPoint> points = this.getPoints();
        for (LayerPoint point : points) {
            s = s + point.toString() + "\n";
        }


        return s;
    }

    public boolean isEmpty() {
        return points.isEmpty();
    }

    private void fillCoords() {
        coordsTop.clear();
        coordsBottom.clear();

        for (LayerPoint point : points) {
            try {
                Coordinate c = new Coordinate(point.hole.getCoords().x,
                        point.hole.getCoords().y,
                        point.hole.getTopForSolid(point.solid));
                coordsTop.add(c);
                c = new Coordinate(point.hole.getCoords().x,
                        point.hole.getCoords().y,
                        point.solid.depth);
                coordsBottom.add(c);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            TriangulationResult res = Triangulator.triangulate(coordsTop);
            top = res.triangles;
            bottom = res.triangles;
            perimetrPoints = res.perimetrPoints;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public TriangleMesh getTriangleMesh() {
        fillCoords();

        float[] pointsArray = new float[coordsTop.size() * 3 + coordsBottom.size() * 3];
        int faces[] = new int[top.size() * 6 + bottom.size() * 6 + perimetrPoints.size() * 2 * 6];
        float tex[] = {0};

        int idx = 0;
        for (Coordinate c : coordsTop) {
            pointsArray[idx] = (float) c.x;
            idx++;
            pointsArray[idx] = (float) c.y;
            idx++;
            pointsArray[idx] = (float) c.z;
            idx++;
        }
        for (Coordinate c : coordsBottom) {
            pointsArray[idx] = (float) c.x;
            idx++;
            pointsArray[idx] = (float) c.y;
            idx++;
            pointsArray[idx] = (float)( c.z +0.001);
            idx++;
        }

        idx = 0;
        for (Triangle tr : top) {
            faces[idx] = tr.p[0];
            idx++;
            faces[idx] = 0;
            idx++;
            faces[idx] = tr.p[1];
            idx++;
            faces[idx] = 0;
            idx++;
            faces[idx] = tr.p[2];
            idx++;
            faces[idx] = 0;
            idx++;
        }
        for (Triangle tr : bottom) {
            faces[idx] = tr.p[0] + coordsTop.size();
            idx++;
            faces[idx] = 0;
            idx++;
            faces[idx] = tr.p[1] + coordsTop.size();
            idx++;
            faces[idx] = 0;
            idx++;
            faces[idx] = tr.p[2] + coordsTop.size();
            idx++;
            faces[idx] = 0;
            idx++;
        }

        for (SideInfo info : perimetrPoints) {
            // первый боковой треугольник
            faces[idx] = info.pointIdx1;
            idx++;
            faces[idx] = 0;
            idx++;

            faces[idx] = info.pointIdx1 + coordsTop.size();
            idx++;
            faces[idx] = 0;
            idx++;

            faces[idx] = info.pointIdx2;
            idx++;
            faces[idx] = 0;
            idx++;

            // второй боковой треугольник
            faces[idx] = info.pointIdx2;
            idx++;
            faces[idx] = 0;
            idx++;


            faces[idx] = info.pointIdx1 + coordsTop.size();
            idx++;
            faces[idx] = 0;
            idx++;

            faces[idx] = info.pointIdx2 + coordsTop.size();
            idx++;
            faces[idx] = 0;
            idx++;
        }


/*
        PhongMaterial material = new PhongMaterial();
        material.setSpecularColor(new Color(Math.random(), Math.random(), Math.random(), 1));
        material.setDiffuseColor(new Color(Math.random(), Math.random(), Math.random(), 1));
*/


        TriangleMesh mesh = new TriangleMesh();
        mesh.getPoints().addAll(pointsArray);
        mesh.getTexCoords().addAll(tex);
        mesh.getFaces().addAll(faces);


        return mesh;
    }

    public boolean contains(Hole hole) {
        for(LayerPoint point: this.getPoints()) {
            if(point.hole.equals(hole)) {
                return true;
            }
        }
        return false;
    }
}
