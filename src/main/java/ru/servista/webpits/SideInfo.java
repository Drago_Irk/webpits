package ru.servista.webpits;

/**
 * Created by user on 01.03.2016.
 */
public class SideInfo {
    public int pointIdx1;
    public int pointIdx2;

    SideInfo(int idx1, int idx2) {
        pointIdx1 = idx1;
        pointIdx2 = idx2;
    }
}
